import org.apache.spark._
import org.apache.log4j.{Level, Logger}
import java.time._
import java.time.format.DateTimeFormatter

import scala.math.Ordering.Implicits._
import org.apache.spark.rdd.RDD

import scala.math.{max, pow, sqrt}

object Main {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org.apache.spark").setLevel(Level.WARN)
    Logger.getLogger("org.spark-project").setLevel(Level.WARN)
    val Seq(masterURL, tripDataPath, stationDataPath) = args.toSeq
    val cfg = new SparkConf().setAppName("Test").setMaster(masterURL)
    //val cfg = new SparkConf().setAppName("Test").setMaster("local[2]")
    val sc = new SparkContext(cfg)
    //val tripData = sc.textFile("file:///D:/data/trips.csv")
    val tripData = sc.textFile(tripDataPath)
    val trips = tripData.map(row=>row.split(",",-1))
    //val stationData = sc.textFile("file:///D:/data/stations.csv")
    val stationData = sc.textFile(stationDataPath)
    val stations = stationData.map(row=>row.split(",",-1))

    case class Station(
                        stationId:Integer,
                        name:String,
                        lat:Double,
                        long:Double,
                        dockcount:Integer,
                        landmark:String,
                        installation:String,
                        notes:String)
    case class Trip(
                     tripId:Integer,
                     duration:Integer,
                     startDate:LocalDateTime,
                     startStation:String,
                     startTerminal:Integer,
                     endDate:LocalDateTime,
                     endStation:String,
                     endTerminal:Integer,
                     bikeId: Integer,
                     subscriptionType: String,
                     zipCode: String)

    val tripsInternal = trips.mapPartitions(rows => {
      val timeFormat =
        DateTimeFormatter.ofPattern("yyyy-M-d H:m")
      rows.map( row =>
        new Trip(
          tripId=row(0).toInt,
          duration=row(1).toInt,
          startDate= LocalDateTime.parse(row(2), timeFormat),
          startStation=row(3),
          startTerminal=row(4).toInt,
          endDate=LocalDateTime.parse(row(5), timeFormat),
          endStation=row(6),
          endTerminal=row(7).toInt,
          bikeId=row(8).toInt,
          subscriptionType=row(9),
          zipCode=row(10)))})

    println(tripsInternal.first)
    println(tripsInternal.first.startStation)

    val stationsInternal = stations.map(row=>
      new Station(
        stationId=row(0).toInt,
        name=row(1),
        lat=row(2).toDouble,
        long=row(3).toDouble,
        dockcount=row(4).toInt,
        landmark=row(5),
        installation=row(6),
        notes=null))

    def findBikeWithMaxDuration(trips: RDD[Trip], stations: RDD[Station]): Int = {
      println("\nfindBikeWithMaxDuration")
      val bikeWithDuration = trips
        .map(trip => (trip.bikeId, trip.duration))
        .reduceByKey(_+_)
        .sortBy(_._2, ascending = false)
        .first
      printf("bikeId = %s, duration = %s\n", bikeWithDuration._1, bikeWithDuration._2)

      bikeWithDuration._1
    }

    def findUsersWhoSpentMoreThan3HoursOnTrips(trips: RDD[Trip], stations: RDD[Station]): Unit = {
      println("\nfindUsersWhoSpentMoreThan3HoursOnTrips")

      trips
        .keyBy(_.zipCode)
        .mapValues(_.duration)
        .reduceByKey(_ + _)
        .filter(_._2 > 3 * 60 * 60)
        .foreach(
          v => printf("user \"%s\" spent %s seconds\n", v._1, v._2)
        )
    }
    implicit val localDateOrdering: Ordering[LocalDateTime] = Ordering.by(x => x.atZone(ZoneId.of("UTC")).toEpochSecond)

    // Найти велосипед с максимальным пробегом.
    val bikeWithMaxDuration = tripsInternal.map(trip => (trip.bikeId, trip.duration))
      .reduceByKey((a, b) => a + b).sortBy(x => x._2, false).first()
    println("ID велосипеда с максимальным пробегом: ", bikeWithMaxDuration._1)

    // Найти наибольшее расстояние между станциями.
    val greatestDistanceBetweenStations = stationsInternal.cartesian(stationsInternal)
      .map(dist => (dist._1.name, dist._2.name, sqrt(pow(dist._2.lat - dist._1.lat, 2) + pow(dist._2.long - dist._1.long, 2)))).sortBy(_._3, ascending = false).first()
    println("Наибольшее расстояние ", greatestDistanceBetweenStations._3, " между станциями ", greatestDistanceBetweenStations._1, " и ", greatestDistanceBetweenStations._2)

    // Найти количество велосипедов в системе.
    val numberOfBicycles = tripsInternal.keyBy(_.bikeId).groupByKey().count()
    println("Количество велосипедов в системе: ", numberOfBicycles)

    // Найти пользователей потративших на поездки более 3 часов.
    val usersWithMoreTreeHours = tripsInternal.map(user => (user.zipCode, user.duration))
      .reduceByKey((a, b) => a + b).filter(dur => dur._2 > 10800)
    println("Список пользователей потративших на поездки более 3 часов:")
    usersWithMoreTreeHours.foreach(println)

    // Найти путь велосипеда с максимальным пробегом через станции.
    val bike = tripsInternal.map(trip => (trip.bikeId, trip.duration))
      .reduceByKey((a, b) => a + b).sortBy(x => x._2, false).map(_._1).first()
    val pathBikeWithMaxDuration = tripsInternal.keyBy(_.bikeId).filter(x => x._1 == bike)
      .sortBy(_._2.startDate)
    println("Путь велосипеда с максимальным пробегом через станции:")
    for (path <- pathBikeWithMaxDuration) {
      println(path._2.startStation, "====>", path._2.endStation)
    }


    sc.stop()
  }
}
